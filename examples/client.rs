use std::fs;

use bytes::Bytes;

use harlequinn::{Certificate, EndpointEvent, HqEndpoint, MessageOrder};

fn main() {
    let mut endpoint = HqEndpoint::new_client("example-protocol");

    let certificate_der = fs::read("./cert.der").unwrap();
    let certificate = Certificate::from_der(&certificate_der).unwrap();
    let socket_addr = "127.0.0.1:9001".parse().unwrap();
    endpoint.connect(socket_addr, "localhost", certificate);

    let mut events = Vec::new();

    loop {
        endpoint.poll_events(&mut events);

        for event in events.drain(..) {
            match event {
                EndpointEvent::ConnectionRequested {
                    peer_id,
                    socket_addr,
                    ..
                } => {
                    endpoint.accept(peer_id);
                    println!("Server connected: {}", socket_addr);

                    endpoint.send_message(
                        peer_id,
                        Bytes::from(&[1, 2, 3, 4][..]),
                        MessageOrder::Unordered,
                    );
                }
                EndpointEvent::Disconnected { .. } => {
                    println!("Server disconnected");
                }
                _ => {}
            }
        }

        std::thread::sleep(std::time::Duration::from_millis(100));
    }
}