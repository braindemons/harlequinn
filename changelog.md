# Changelog

## Next Release

- Only raise Disconnect event if ConnectionRequested was raised


## 0.1.1

- Fix incorrect endpoint closing behavior


## 0.1.0

Initial release