# Harlequinn

Harlequinn is a real-time networking library primarily aimed at games.
It is based on the [Quinn](https://github.com/djc/quinn) implementation of the QUIC protocol.

Harlequinn wraps around Quinn to provide an API that can be easily used from a synchronous game
loop.
It also implements some additional nice-to-have features out of the box.


# Features

A lot of features provided by Harlequinn are implemented as part of the QUIC implementation by
Quinn.
For convenience notable features implemented by Quinn are listed here as well.

- Connection management
- Connection verification/rejection
- Unreliable messages (QUIC datagrams)
- Reliable ordered and unordered messages (QUIC streams)
- Fragmentation (only for reliable messages)
- Protocol verification handshake
- Slowloris/DoS mitigation
- TLS encryption


## To-Do:

- TOFU certificate confirmation
- Improved connection close reason API
- Multiple separate reliable ordered streams


## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
