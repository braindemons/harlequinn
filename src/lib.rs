//! Harlequinn is a real-time networking library primarily aimed at games.
//! It is based on the [Quinn](https://github.com/djc/quinn) implementation of the QUIC protocol.
//!
//! Harlequinn wraps around Quinn to provide an API that can be easily used from a synchronous game
//! loop.
//! It also implements some additional nice-to-have features out of the box.
//! 
//! `HqEndpoint` is the entry point of this library.

#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod endpoint;
mod worker;

pub use self::endpoint::{HqEndpoint, EndpointEvent};

pub use quinn::{Certificate, PrivateKey};

use slotmap::new_key_type;

new_key_type! {
    /// Represents a peer connected to an `HqEndpoint`.
    pub struct PeerId;
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MessageOrder {
    Unordered,
    Ordered,
}